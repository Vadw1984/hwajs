// Комментарии:
//
//
//     Это задача на ООП. Вам нужно сделать класс, который получает на вход информацию о гамбургере, и на выходе дает информацию о весе и цене. Никакого взаимодействия с пользователем и внешним миром класс делать не должен - все нужные данные ему необходимо передать явно. Он ничего не будет спрашивать, и не будет ничего выводить.
//
//
//     Почему? Потому что каждый должен заниматься своим делом, класс должен только обсчитывать гамбургер, а вводом-выводом должны заниматься другие части кода. Иначе мы получим кашу, где разные функции смешаны вместе.
//
//
//     Типы начинок, размеры надо сделать константами. Никаких магических строк не должно быть.
//
//
//     Переданную информацию о параметрах гамбургера экземпляр класса хранит внутри в своих полях. Вот как может выглядеть использование этого класса:
// // маленький гамбургер с начинкой из сыра
//     var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // добавка из майонеза
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
// console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
// console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // А сколько теперь стоит?
// console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
// console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// // Убрать добавку
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// console.log("Have %d toppings", hamburger.getToppings().length); // 1
/**
 // * ===========================================================================
 * Класс, объекты которого описывают параметры гамбургера.
 ** @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(options) {
    let {size, stuffing, topping} = options;

    this.size = size;
    this.stuffing = stuffing;
    this.topping = topping || [];

    // Object.assign(this,options);
    // console.log(this);

    if(!size){
        throw new Error('HamburgerException: no size given')
    } else if(!stuffing){
        throw new Error('HamburgerException: no stuffing given')
    } else if(!topping){
        throw new Error('HamburgerException: no topping given')
    }
}
/* Размеры, виды начинок и добавок */
// const cost = {
// }
Hamburger.hamburgerProps = {
    SIZE_SMALL : {size:'small',price:50,calories:20},
    SIZE_LARGE : {size:'Large',price:100,calories:40},
    STUFFING_CHEESE : {stuff:'cheese', price:10,calories:20},
    STUFFING_SALAD : {stuff:'salad', price:20,calories:5},
    STUFFING_POTATO : {stuff:'mayo',price:15,calories:10},
    TOPPING_MAYO : {named:'mayo', price:20, calories:5},
    TOPPING_SPICE : {named:'spice',price:15, calories:0}
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if(!topping){
        throw new Error('topping is require')
    }
    try{
        this.topping.forEach(el => {
            if(el.named===topping.named){
                throw new Error()
            }
        });
        this.topping.push(topping)
    }
    catch (e) {
        console.log( "HamburgerException: topping has almost used with this hamburger, duplicate " + topping.named);
    }
};
/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
       this.topping.forEach(el => {
        if(el.named===topping.named){
            this.topping.splice(this.topping.indexOf(el),1);
        }
    })
 };

/**
 * Получить список добавок.
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    this.topping.forEach(element=>console.log(element));
    };
  /**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    console.log(this.size.size);
};
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    console.log(this.stuffing.stuff);
};
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let calcPriceTopping = 0;
    this.topping.forEach(elem => {
        calcPriceTopping += elem.price;
    });

    let calcPriceAll = this.size.price+this.stuffing.price+calcPriceTopping;
    console.log(calcPriceAll);
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let calcCalorTopping = 0;
    this.topping.forEach(elem => {
        calcCalorTopping += elem.calories;
    });
    let calcCalorAll = this.size.calories+this.stuffing.calories+calcCalorTopping;
    console.log(calcCalorAll);
};
/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException () { }

// create object
const hamburgerCopy = new Hamburger({size:Hamburger.hamburgerProps.SIZE_SMALL, stuffing:Hamburger.hamburgerProps.STUFFING_CHEESE,topping:[]});
console.log(hamburgerCopy);

// add topping
hamburgerCopy.addTopping(Hamburger.hamburgerProps.TOPPING_MAYO);
hamburgerCopy.addTopping(Hamburger.hamburgerProps.TOPPING_SPICE);
hamburgerCopy.addTopping(Hamburger.hamburgerProps.TOPPING_MAYO);

// remove topping
hamburgerCopy.removeTopping(Hamburger.hamburgerProps.TOPPING_MAYO);

// get info about ham
hamburgerCopy.getSize();
hamburgerCopy.getStuffing();
hamburgerCopy.getToppings();

// calculation
hamburgerCopy.calculatePrice();
hamburgerCopy.calculateCalories();

// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // HamburgerException: duplicate topping 'TOPPING_MAYO'
