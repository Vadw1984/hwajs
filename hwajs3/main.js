import CreateTable from "./table.js";
import PlayGame from "./game.js";

class LetsStartGame{
    constructor(speed){
        this.speed=speed;
        this.elemArr = [...document.getElementsByTagName('div')];
        this.scope = document.getElementsByTagName('p')[0];
    }
    clearSota=()=>{
        this.elemArr.forEach((e)=>e.classList.remove('player'));
        this.elemArr.forEach((e)=>e.classList.remove('computer'));
        this.elemArr.forEach((e)=>e.classList.remove('active'));
        this.elemArr.forEach((e)=>e.classList.add('passive'));
     };

    displayScope=()=>{
        this.computerScope = [...document.getElementsByClassName('computer')];
        this.playerScope = [...document.getElementsByClassName('player')];
        this.scope.innerHTML=this.playerScope.length+' : '+this.computerScope.length;
        console.log(this.computerScope.length);
    };
    whoIsWinner=()=>{
        if(this.computerScope.length > this.elemArr.length/2){
            alert(this.scope.innerHTML = this.playerScope.length + ' : '+this.computerScope.length +'Computer Win!!!');
            this.clearSota();
        } else if (this.playerScope.length > this.elemArr.length/2){
            alert(this.scope.innerHTML=this.playerScope+' : '+this.computerScope +'Player Win!!!');
            this.clearSota();
        }
    };
    levelSpeed=()=>{
        timerId = setInterval( () => {
            let newGame = new PlayGame(this.speed);
            newGame.getElemArr();
            this.displayScope();
            this.whoIsWinner();
        }, this.speed);
        console.log(timerId);
    };
    start=()=>{
        clearInterval(timerId);
        this.clearSota();
        this.levelSpeed(this.speed)
    }
}

let newTable = new CreateTable(10);
newTable.createField();
let timerId;

// // --------------------------------------------
let levelEasy=document.getElementsByTagName('button')[0];
let levelMedium=document.getElementsByTagName('button')[1];
let levelHard=document.getElementsByTagName('button')[2];

levelEasy.addEventListener('click', ()=>{
    let startGame = new LetsStartGame(1500);
    startGame.start();
});
levelMedium.addEventListener('click', ()=>{
    let startGame1 = new LetsStartGame(1000);
    startGame1.start();
});
levelHard.addEventListener('click', ()=>{
    let startGame2 = new LetsStartGame(500);
    startGame2.start();
});

// ----------------------------------

//
//
// let timerId;
// let speed=0;
//
// levelEasy.addEventListener('click', ()=> {
//     speed=1500;
//     clearSota();
//     clearInterval(timerId);
//     levelSpeed(speed);
//
// });
//
// levelMedium.addEventListener('click', ()=>{
//     speed=1000;
//     clearSota();
//     clearInterval(timerId);
//     levelSpeed(speed);
//
// });
//
// levelHard.addEventListener('click', ()=>{
//     speed=500;
//     clearSota();
//     clearInterval(timerId);
//     levelSpeed(speed);
//
// });
//
// function levelSpeed(speed){
//     timerId = setInterval( () => {
//         let newGame = new PlayGame(speed);
//         newGame.getElemArr();
//         displayScope();
//         whoIsWinner()
//     }, speed);
// }
// let elemArr = [...document.getElementsByTagName('div')];
// // console.log(elemArr.length);
//
// function clearSota(){
//     elemArr.forEach((e)=>e.classList.remove('player'));
//     elemArr.forEach((e)=>e.classList.remove('computer'));
//     elemArr.forEach((e)=>e.classList.remove('active'));
//     elemArr.forEach((e)=>e.classList.add('passive'));
// }
//
// function whoIsWinner(){
//     let computerScope = [...document.getElementsByClassName('computer')];
//     let playerScope = [...document.getElementsByClassName('player')];
//     let scope = document.getElementsByTagName('p')[0];
//
//     // console.log(scope);
//     // console.log(computerScope.length);
//     // console.log(elemArr.length);
//
//     if(computerScope.length > elemArr.length/2){
//         scope.innerHTML = playerScope + ' : '+computerScope +'Computer Win!!!';
//         clearSota();
//     } else if (playerScope.length > elemArr.length/2){
//         scope.innerHTML=playerScope+' : '+computerScope +'Player Win!!!';
//         clearSota();
//     }
// }
//
// let displayScope = function(){
//     let scope = document.getElementsByTagName('p')[0];
//     let computerScope = [...document.getElementsByClassName('computer')].length;
//     let playerScope = [...document.getElementsByClassName('player')].length;
//     // console.log(computerScope);
//     scope.innerHTML=playerScope+' : '+computerScope;
// };
//


