export default class PlayGame {
    constructor(speed) {
        this.speed = speed;
        this.elemArr = [...document.getElementsByClassName('passive')];
    }
       getElemArr = () => {
        let randomElem = this.elemArr[Math.floor(Math.random()*this.elemArr.length)];
        randomElem.classList.remove('passive');
           randomElem.classList.add('active');

           randomElem.addEventListener('click', () => {
               if (randomElem.classList.value === 'active') {
                   randomElem.classList.remove('active');
                   randomElem.classList.add('player')
               }
        });

           setTimeout(() => {
            if (randomElem.classList.value !== 'player') {
                randomElem.classList.remove('active');
                randomElem.classList.add('computer');
            }
        }, this.speed-10);
    };
}




