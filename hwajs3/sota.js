export default class CreateSota {
    constructor(tag, size) {
        this.tag = tag;
        this.size = size;
        this.sota =  document.createElement(this.tag);
    }
    createSotaElement = ()=>{
        this.sota.style.width = this.size+'px';
        this.sota.style.height = this.size+'px';
        this.sota.style.border = '1px solid black';
        this.sota.classList.add('passive');
        return this.sota
    };
}


