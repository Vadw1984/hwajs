import CreateSota from "./sota.js";

export default class CreateTable {
    constructor(numOfSota) {
        this.size=numOfSota;
        this.body = document.getElementsByTagName('body')[0];
        this.container = document.createElement('main');
    }
    createContainer=()=>{
        this.body.appendChild(this.container);
        this.container.style.flexDirection = 'column';
        this.container.style.flexWrap = 'wrap';
        this.container.style.display = 'flex';
        this.container.style.width = this.size*this.size+this.size*2+'px';
        this.container.style.height = this.size*this.size+this.size*2+'px';
    };
    createField = () => {
        this.createContainer();
        for (let i = 0; i <= this.size * this.size-1; i++) {
            let newSota = new CreateSota('div',10);
            newSota.createSotaElement();
            this.container.appendChild(newSota.sota);
        }
    };
}